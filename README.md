# voxl-vpn

Helper utility to wrap the OpenVPN client on VOXL

## Usage

### voxl-vpn-init

VOXL VPN Initialization.  Initializes certificates and configures services to run on startup

```bash
voxl-vpn-init [domain] [token]

Options:
--reset		Removes keys and resets VPN configuration to default.
--help		Displays this message.
```

## Building

### IPK Download

Visit [here](http://voxl-packages.com/stable) for a prebuilt binary.

### Making voxl-vpn IPK

```bash
./make_package.sh
```

### Installing on VOXL

```bash
./install_on_voxl.sh
```

### Cleaning Build

```bash
sudo ./clean.sh
```
