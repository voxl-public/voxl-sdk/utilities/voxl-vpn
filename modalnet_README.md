# ModalNet VPN

To quickly and securely connect your LTE enabled vehicle to a ground control station (e.g. QGroundControl) over the internet, the ModalNet VPN can be used.  

The ModalAI ModalNet VPN leverages OpenVPN and simplifies the in field setup while providing hosted or on-premise solutions based on your needs.

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

- [Summary](#summary)
- [Setup](#setup)
  - [Account Creation](#account-creation)
  - [Certificate Creation](#certificate-creation)
- [Connecting VOXL to VPN](#connecting-voxl-to-vpn)
  - [LTE Setup](#lte-setup)
  - [Install Certificate](#install-certificate)
- [Connecting Ubuntu GCS to VPN](#connecting-ubuntu-gcs-to-vpn)
- [Connecting Android Tablet to VPN](#connecting-android-tablet-to-vpn)
- [Technical Details](#technical-details)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Summary

In short, this guide will show you how to:

- create an account at [vpn001.modalai.com/signup](https://vpn001.modalai.com/signup)
  - NOTE: requires an `@modalai.com` email or a special invite
- create two certificates, one for a vehicle, one for a GCS
- show you how to install and configure on a vehicle with a sigle command
- show you download certificate to your computer or tablet and use an OpenVPN Client
- confirm a connection is up

## Setup

### Account Creation

Create an account at [vpn001.modalai.com/signup](https://vpn001.modalai.com/signup)

  - **BETA USER NOTES -->** all beta users are sharing this `vpn001` instance.  In the future, it can be an instance per customer

Once you sign up you will need to confirm your email with the link that is sent to you.

After email confirmation you will be able to start creating certificates.

### Certificate Creation

Navigate to `vpn001.modalai.com` to start creating your VPN certificates.

#### Certificate for VOXL

In order to create a new certificate file for Drone/VOXL:

1. Click `Create New Certificate`
2. Enter a **Name**, e.g. `m500`
3. In the **Type** drop down list, select `Vehicle (VOXL)`
4. Click `Save` to create the certificate
5. You should now see your newly created certificate listed on the Certificates page.

#### Certificate for GCS

Folow the same procedure as creating a certificate for VOXL, but for **Type**, select `Ground Control Station`

## Connecting VOXL to VPN

### LTE Setup

On VOXL, the LTE connection will need to be configured before attempting to connect to the VPN. This can be enabled using the `voxl-configure-modem` LTE setup script from the [voxl-modem](https://gitlab.com/voxl-public/voxl-modem) software package.

### Install Certificate and Configure System

The `voxl-vpn` utility is available on VOXL.  It allows you to easily download a certifcate and configure the system to automatically connect on power up.

In order to connect to the VPN on VOXL:

1. Next to the certificate that you would like to use, click `Install on VOXL`

2. A window will pop up with instructions on how to connect to the VPN

3. Open a terminal and enter the following to connect to VOXL

```
adb shell
```

4. Copy and paste the `voxl-vpn-init` command from the window into the terminal and press enter, for example:

```bash
voxl-vpn-init https://vpn001.modalai.com 000ba4bc27bf678e12e063c9fed9000

Downloading key to /etc/openvpn...
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  4998    0  4998    0     0   7897      0 --:--:-- --:--:-- --:--:--  9153
curl: Saved to filename 'modalai-5.ovpn'

Enabled OpenVPN connection
```

If setup was done correctly, VOXL should now connect to the VPN. You can confirm the connection by running

```
/ # ifconfig
...

tun0      Link encap:UNSPEC  HWaddr 00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00
          inet addr:192.168.255.5  P-t-P:192.168.255.5
...
```

and looking for the `tun0` network interface in the list.

You'll find an address similar to `192.168.255.5`, this is your static IP address on the vehicle.

## Connecting Ubuntu GCS to VPN

In order to connect an Ubuntu Ground Station to the VPN you will need to install OpenVPN on the device.

This can be done using the following:

```
sudo apt-get install openvpn
```

1. After creating a certificate for the Ubuntu GCS, download it to the machine by clicking on the `Download` button next to the corresponding certifcate.

2. Start the connection to the VPN by using the following:

```
sudo openvpn --config PATH_TO_CERT
```

where `PATH_TO_CERT` is the path to the downloaded certificate file.  You should see at the end of initialization:

```bash
...
Thu Aug 27 14:06:13 2020 Initialization Sequence Completed
```

Again, checking for interfaces, you should have a new tunnel:

```bash
ip addr

...
4: tun0: <POINTOPOINT,MULTICAST,NOARP,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UNKNOWN group default qlen 100
    link/none 
    inet 192.168.255.3/24 brd 192.168.255.255 scope global tun0
...
```

## Connecting Android Tablet to VPN

In order to connect an Android tablet to the VPN, you will need to install the [OpenVPN Connect](https://play.google.com/store/apps/details?id=net.openvpn.openvpn&hl=en_US) application from the Google Play Store.

1. In your tablet's browser, navigate to the Certificates page

2. Create a certificate for the tablet

3. Press `Download` next to the corresponding certificate

4. On the bottom of your screen you should see a popup window when the download is complete

<div align="center">
<img src="media/cert-download.jpg"  width="550" height="50">
</div>

5. On the popup window, press `Open`

6. You will be asked whether you would like to open the file using OpenVPN Connect, press `Yes`

7. OpenVPN Connect will open and ask if you would like to import the .ovpn profile from the certificate, press `OK`

<div align="center">
<img src="media/cert-import.jpg"  width="412" height="152">
</div>

8. Check the box labeled `Connect after import` and press the `ADD` button on the top right of the screen

<div align="center">
<img src="media/cert-connect.jpg"  width="600" height="250">
</div>

The tablet will now be connected to the VPN, you will be able to monitor the data I/O from this screen as well as view the IP address assigned to the tablet by the VPN.

<div align="center">
<img src="media/openvpn-connect.jpg"  width="450" height="675">
</div>

`YOUR PRIVATE IP` will be the IP address for your ground station when connecting to your VOXL.

## Technical Details

| Component              | Version |
|---                     |--- |
| OpenVPN Server         | v2.4.8, Docker, Ubuntu 18.04 |
| OpenVPN VOXL Client    | voxl-vpn, v2.1.3, system-image-TBD  |
| Tested Android Client  | OpenVPN Connect ver 3.2.2 |
